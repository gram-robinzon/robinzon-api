<?php

namespace App\Tests\Controller;

use App\Entity\Locale;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @covers \App\Controller\ExpansionController
 */
class ExpansionControllerTest extends WebTestCase
{

    public function provider()
    {
        return [
            [Locale::UK, 'mdpi', 'normal', '3'],
            [Locale::UK, 'mdpi', 'large', '3'],
            [Locale::UK, 'mdpi', 'xlarge', '3'],
            [Locale::UK, 'hdpi', 'normal', '3'],
            [Locale::UK, 'hdpi', 'large', '3'],
            [Locale::UK, 'hdpi', 'xlarge', '3'],
            [Locale::UK, 'xhdpi', 'normal', '3'],
            [Locale::UK, 'xhdpi', 'large', '3'],
            [Locale::UK, 'xhdpi', 'xlarge', '3'],
            [Locale::UK, 'xxhdpi', 'normal', '3'],
            [Locale::UK, 'xxhdpi', 'large', '3'],
            [Locale::UK, 'xxhdpi', 'xlarge', '3'],
            [Locale::UK, 'xxxhdpi', 'normal', '3'],
            [Locale::UK, 'xxxhdpi', 'large', '3'],
            [Locale::UK, 'xxxhdpi', 'xlarge', '3'],
            [Locale::RU, 'mdpi', 'normal', '4'],
            [Locale::RU, 'mdpi', 'large', '4'],
            [Locale::RU, 'mdpi', 'xlarge', '4'],
            [Locale::RU, 'hdpi', 'normal', '4'],
            [Locale::RU, 'hdpi', 'large', '4'],
            [Locale::RU, 'hdpi', 'xlarge', '4'],
            [Locale::RU, 'xhdpi', 'normal', '4'],
            [Locale::RU, 'xhdpi', 'large', '4'],
            [Locale::RU, 'xhdpi', 'xlarge', '4'],
            [Locale::RU, 'xxhdpi', 'normal', '4'],
            [Locale::RU, 'xxhdpi', 'large', '4'],
            [Locale::RU, 'xxhdpi', 'xlarge', '4'],
            [Locale::RU, 'xxxhdpi', 'normal', '4'],
            [Locale::RU, 'xxxhdpi', 'large', '4'],
            [Locale::RU, 'xxxhdpi', 'xlarge', '4'],
        ];
    }

    /**
     * @dataProvider provider
     *
     * @param $locale
     * @param $dpi
     * @param $device
     * @param $version
     */
    public function testGetAction($locale, $dpi, $device, $version)
    {
        $client = self::createClient();

        $client->request('GET', "/api/v1/$locale/$device/$dpi/expansions/$version");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['url']), 'Missing url');
        $this->assertTrue(isset($content['version']), 'Missing version');
        $this->assertTrue(isset($content['id']), 'Missing id');
    }
}