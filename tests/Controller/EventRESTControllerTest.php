<?php

namespace App\Tests\Controller;

use App\Entity\Locale;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @covers \App\Controller\EventRESTController
 */
class EventRESTControllerTest extends WebTestCase
{
    public function localeProvider()
    {
        return [
            [Locale::UK],
            [Locale::RU],
        ];
    }

    /**
     * @dataProvider localeProvider
     */
    public function testGetsAction($locale)
    {
        $client = self::createClient();

        $client->request('GET', "/api/v1/$locale/events");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['total']), 'Missing total');
        $this->assertTrue(isset($content['count']), 'Missing count');
        $this->assertTrue(isset($content['items']), 'Missing items');
    }
}