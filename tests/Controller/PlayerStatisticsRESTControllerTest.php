<?php

namespace App\Tests\Controller;

use App\Entity\Locale;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @covers \App\Controller\PlayerStatisticsRESTController
 */
class PlayerStatisticsRESTControllerTest extends WebTestCase
{
    public function localeProvider()
    {
        return [
            [Locale::UK],
            [Locale::RU],
        ];
    }

    /**
     * @dataProvider localeProvider
     */
    public function testGetAction($locale)
    {
        $client = self::createClient();

        $name = md5(uniqid());
        $device = md5(uniqid());

        $client->request('GET', "/api/v1/$locale/players/$name/devices/$device/statistics");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['statistics']), 'Missing statistics');
        $this->assertTrue(isset($content['categories']), 'Missing categories');
        $this->assertTrue(isset($content['playerEvents']), 'Missing playerEvents');

        $this->assertTrue(isset($content['categories']['total']), 'Missing total');
        $this->assertTrue(isset($content['categories']['count']), 'Missing count');
        $this->assertTrue(isset($content['categories']['items']), 'Missing items');
        $this->assertEquals($content['categories']['count'], count($content['categories']['items']), 'Invalid count');

        $this->assertTrue(isset($content['statistics']['total']), 'Missing total');
        $this->assertTrue(isset($content['statistics']['count']), 'Missing count');
        $this->assertTrue(isset($content['statistics']['items']), 'Missing items');
        $this->assertEquals($content['statistics']['count'], count($content['statistics']['items']), 'Invalid count');

        $this->assertTrue(isset($content['playerEvents']['total']), 'Missing total');
        $this->assertTrue(isset($content['playerEvents']['count']), 'Missing count');
        $this->assertTrue(isset($content['playerEvents']['items']), 'Missing items');
        $this->assertEquals($content['playerEvents']['count'], count($content['playerEvents']['items']), 'Invalid count');
    }

    /**
     * @dataProvider localeProvider
     */
    public function testGetsAction($locale)
    {
        $client = self::createClient();

        $client->request('GET', "/api/v1/$locale/statistics");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['statistics']), 'Missing statistics');
        $this->assertTrue(isset($content['categories']), 'Missing categories');

        $this->assertTrue(isset($content['statistics']['total']), 'Missing total');
        $this->assertTrue(isset($content['statistics']['count']), 'Missing count');
        $this->assertTrue(isset($content['statistics']['items']), 'Missing items');
        $this->assertEquals($content['statistics']['count'], count($content['statistics']['items']), 'Invalid count');

        $this->assertTrue(isset($content['categories']['total']), 'Missing total');
        $this->assertTrue(isset($content['categories']['count']), 'Missing count');
        $this->assertTrue(isset($content['categories']['items']), 'Missing items');
        $this->assertEquals($content['categories']['count'], count($content['categories']['items']), 'Invalid count');

        foreach ($content['categories']['items'] as $category) {
            $this->assertTrue(isset($category['events']), 'Missing category.events');
            $this->assertGreaterThan(0, count($category['events']), 'Invalid category.events');
        }
    }

    /**
     * @dataProvider localeProvider
     */
    public function testGets_has_expected_category_count($locale)
    {
        $client = self::createClient();

        $client->request('GET', "/api/v1/$locale/statistics");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['categories']), 'Missing categories');

        $this->assertTrue(isset($content['categories']['count']), 'Missing count');

        switch ($locale) {
            case Locale::RU:
                $this->assertEquals(18, $content['categories']['count'], 'Invalid count');
                break;
            case Locale::UK:
                $this->assertEquals(17, $content['categories']['count'], 'Invalid count');
                break;
        }
    }

    /**
     * @dataProvider localeProvider
     */
    public function testGets_has_expected_statistics_count($locale)
    {
        $client = self::createClient();

        $client->request('GET', "/api/v1/$locale/statistics");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['statistics']), 'Missing statistics');

        $this->assertTrue(isset($content['statistics']['count']), 'Missing count');

        switch ($locale) {
            case Locale::RU:
                $this->assertEquals(58, $content['statistics']['count'], 'Invalid statistics count');
                break;
            case Locale::UK:
                $this->assertEquals(56, $content['statistics']['count'], 'Invalid statistics count');
                break;
        }
    }

    /**
     * @dataProvider localeProvider
     */
    public function testGets_statistic_items_are_present_in_category_list($locale)
    {
        $client = self::createClient();

        $client->request('GET', "/api/v1/$locale/statistics");

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['statistics']), 'Missing statistics');
        $this->assertTrue(isset($content['categories']), 'Missing categories');

        $this->assertTrue(isset($content['statistics']['items']), 'Missing statistics.items');
        $this->assertTrue(isset($content['categories']['items']), 'Missing categories.items');

        $eventRegistry = [];
        $categoryRegistry = [];

        foreach ($content['categories']['items'] as $category) {

            $categoryKey = $category['id'];

            $categoryRegistry[$categoryKey] = true;

            foreach ($category['events'] as $event) {
                $eventKey = $event['id'];

                $eventRegistry[$eventKey] = true;
            }
        }

        foreach ($content['statistics']['items'] as $stat) {
            $eventKey = $stat['event']['id'];
            $categoryKey = $stat['category']['id'];

            $this->assertTrue(isset($eventRegistry[$eventKey]),
                "Event is not present in category list for event $eventKey and category $categoryKey");
            $this->assertTrue(isset($categoryRegistry[$categoryKey]),
                "Category is not found in category list $categoryKey");
        }
    }
}