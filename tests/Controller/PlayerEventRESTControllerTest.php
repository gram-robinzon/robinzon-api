<?php

namespace App\Tests\Controller;

use App\Entity\Event;
use App\Entity\Locale;
use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @covers \App\Controller\PlayerEventRESTController
 */
class PlayerEventRESTControllerTest extends WebTestCase
{
    public function localeProvider()
    {
        return [
            [Locale::UK],
            [Locale::RU],
        ];
    }

    /**
     * @dataProvider localeProvider
     */
    public function testPostAction($locale)
    {
        $client = self::createClient();

        $name = md5(uniqid());
        $device = md5(uniqid());

        $em = $client->getContainer()->get('doctrine')->getManager();

        $event = $em->getRepository(Event::class)->findOneBy([]);
        if (!$event) {
            self::fail("No event found");
        }

        $client->request('POST', "/api/v1/$locale/players/$name/devices/$device/events/{$event->getCode()}", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ]);

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
    }


    /**
     * @dataProvider localeProvider
     */
    public function testPostBatchAction($locale)
    {
        $client = self::createClient();

        $name = md5(uniqid());
        $device = md5(uniqid());

        $em = $client->getContainer()->get('doctrine')->getManager();

        $events = $em->getRepository(Event::class)->findBy([], [], 10);
        if (count($events) !== 10) {
            self::fail("No event found");
        }

        $client->request('POST', "/api/v1/$locale/players/$name/devices/$device/events", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode(array_map(function (Event $event) {
            return [
                'code' => $event->getCode()
            ];
        }, $events)));

        $response = $client->getResponse();

        $this->assertEquals(JsonResponse::HTTP_CREATED, $response->getStatusCode());
    }

}