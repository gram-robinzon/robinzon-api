<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class PlayerStatisticsRepository extends EntityRepository
{
    /**
     * @param array $filter
     * @return array
     */
    public function findByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('category.ordering', 'ASC');

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('stat.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }

    private function createFilterQuery($filter)
    {
        $qb = $this->createQueryBuilder('stat');
        $e = $qb->expr();

        $qb
            ->addSelect('player')
            ->addSelect('event')
            ->addSelect('category')
            ->addSelect('locale');

        $qb
            ->join('stat.player', 'player')
            ->join('stat.event', 'event')
            ->join('event.category', 'category')
            ->join('category.locale', 'locale');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'locale':
                    $qb->andWhere($e->eq('locale.code', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'players':
                    $qb->andWhere($e->in('player.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }
}