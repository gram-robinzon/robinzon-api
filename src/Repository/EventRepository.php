<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class EventRepository extends EntityRepository
{
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('event.id');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('event.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }

    private function createFilterQuery($filter)
    {
        $qb = $this->createQueryBuilder('event');
        $e = $qb->expr();

        $qb->addSelect('category')
            ->addSelect('locale');

        $qb->join('event.category', 'category')
            ->join('category.locale', 'locale');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'locale':
                    $qb->andWhere($e->eq('locale.code', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'code':
                    if (is_array($value)) {
                        $qb->andWhere($e->in('event.code', ":$key"))
                            ->setParameter($key, $value);
                    } else {
                        $qb->andWhere($e->eq('event.code', ":$key"))
                            ->setParameter($key, $value);
                    }
                    break;
            }
        }

        return $qb;
    }
}