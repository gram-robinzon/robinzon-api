<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class PlayerRepository extends EntityRepository
{
    /**
     * @param array $filter
     * @return array
     */
    public function findByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('player.id', 'DESC');

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getResult();
    }

    private function createFilterQuery($filter)
    {
        $qb = $this->createQueryBuilder('player');
        $e = $qb->expr();

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'device':
                    $qb->andWhere($e->eq('player.device', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'name':
                    $qb->andWhere($e->eq('player.name', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('category.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }
}