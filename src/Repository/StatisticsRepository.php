<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class StatisticsRepository extends EntityRepository
{
    /**
     * @param array $filter
     * @return array
     */
    public function findByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('category.ordering', 'ASC');

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('stat.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }

    private function createFilterQuery($filter)
    {
        $qb = $this->createQueryBuilder('stat');
        $e = $qb->expr();

        $qb
            ->addSelect('event')
            ->addSelect('category');

        $qb
            ->join('stat.event', 'event')
            ->join('stat.category', 'category')
            ->join('category.locale', 'locale');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'locale':
                    $qb->andWhere($e->eq('locale.code', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'event':
                    $qb->andWhere($e->eq('event.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'category':
                    $qb->andWhere($e->eq('category.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }
}