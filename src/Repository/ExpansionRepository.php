<?php

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

class ExpansionRepository extends EntityRepository
{
    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('exp.version', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter) . $page . $limit))
            ->getResult();
    }

    private function createFilterQuery($filter)
    {
        $qb = $this->createQueryBuilder('exp');
        $e = $qb->expr();

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'locale':
                    $qb->andWhere($e->eq('exp.locale', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'device':
                    $qb->andWhere($e->eq('exp.device', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'dpi':
                    $qb->andWhere($e->eq('exp.dpi', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'version':
                    $qb->andWhere($e->lte('exp.version', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }
}