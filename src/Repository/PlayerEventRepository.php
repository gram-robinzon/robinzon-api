<?php

namespace App\Repository;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class PlayerEventRepository extends EntityRepository
{
    /**
     * @param array $filter
     * @return array
     */
    public function findByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('category.ordering', 'ASC');

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true)
            ->getResult();
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('playerEvent.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }

    private function createFilterQuery($filter)
    {
        $qb = $this->createQueryBuilder('playerEvent');
        $e = $qb->expr();

        $qb
            ->addSelect('event')
            ->addSelect('category');

        $qb
            ->join('playerEvent.event', 'event')
            ->join('playerEvent.player', 'player')
            ->join('event.category', 'category')
            ->join('category.locale', 'locale');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'locale':
                    $qb->andWhere($e->eq('locale.code', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'players':
                    $qb->andWhere($e->in('player.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'isPrimary':
                    $qb->andWhere($e->eq('playerEvent.isPrimary', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
            }
        }

        return $qb;
    }
}