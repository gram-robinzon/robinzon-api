<?php

namespace App\Service;

use App\Entity\Locale;
use App\Entity\Player;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PlayerService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $device
     * @param string $name
     * @param string $localeName
     * @return Player
     * @throws \Exception
     */
    public function findOrCreate(string $device, string $name, string $localeName): Player
    {
        if (strlen($device) < 10 || strlen($name) < 10) {
            throw new \Exception('Invalid id', 400);
        }

        $em = $this->container->get('doctrine')->getManager();

        /** @var Locale $locale */
        $locale = $em->getRepository(Locale::class)->findOneBy([
            'code' => $localeName
        ]);
        if (!$locale) {
            throw new \Exception('Locale was not found', 404);
        }

        $player = $em->getRepository(Player::class)->findOneBy([
            'device' => $device,
            'name' => $name
        ]);
        if (!$player) {
            $player = new Player();
            $player->setDevice($device);
            $player->setName($name);
        }

        if ($player->getLocale() != $locale) {
            $player->setLocale($locale);
            $em->persist($player);
            $em->flush();
        }

        return $player;
    }

    /**
     * @param string $device
     * @param string $name
     * @param string $localeName
     * @return Player
     * @throws \Exception
     */
    public function find(string $device, string $name, string $localeName): Player
    {
        if (strlen($device) < 10 || strlen($name) < 10) {
            throw new \Exception('Invalid id', 400);
        }

        $em = $this->container->get('doctrine')->getManager();

        /** @var Locale $locale */
        $locale = $em->getRepository(Locale::class)->findOneBy([
            'code' => $localeName
        ]);
        if (!$locale) {
            throw new \Exception('Locale was not found', 404);
        }

        $player = $em->getRepository(Player::class)->findOneBy([
            'device' => $device,
            'name' => $name
        ]);
        if (!$player) {
            throw new \Exception('Player was not found', 404);
        }

        if ($player->getLocale() != $locale) {
            $player->setLocale($locale);
            $em->persist($player);
            $em->flush();
        }

        return $player;
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}