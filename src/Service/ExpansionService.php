<?php

namespace App\Service;

use App\Entity\Expansion;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExpansionService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $filter
     * @return Expansion|null
     */
    public function findOneByFilter($filter)
    {
        $em = $this->container->get('doctrine')->getManager();

        $items = $em->getRepository(Expansion::class)->findByFilter($filter, 1, 1);

        if (count($items) === 0) {
            return null;
        }

        return $items[0];
    }

    public function serialize($content)
    {
        $hostname = $this->container->getParameter('hostname');

        $result = json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);

        if (isset($result['file'])) {
            $result['url'] = $hostname . $result['file'];
        }

        return $result;
    }


}