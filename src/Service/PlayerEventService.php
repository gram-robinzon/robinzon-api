<?php

namespace App\Service;

use App\Entity\Event;
use App\Entity\Player;
use App\Entity\PlayerEvent;
use Doctrine\DBAL\Connection;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PlayerEventService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function remove(Player $player)
    {
        $em = $this->container->get('doctrine')->getManager();

        /** @var Connection $c */
        $c = $em->getConnection();

        $c->prepare('DELETE FROM player_event WHERE player_id = ' . $player->getId())->execute();
    }

    /**
     * @param Player $player
     * @param string $code
     * @return PlayerEvent
     * @throws \Exception
     */
    public function create(Player $player, string $code)
    {
        $em = $this->container->get('doctrine')->getManager();

        $events = $em->getRepository(Event::class)->findByFilter([
            'code' => $code,
            'locale' => $player->getLocale()->getCode()
        ], 1, 1);
        if (count($events) !== 1) {
            throw new \Exception('Event was not found', 404);
        }

        /** @var Event $event */
        $event = $events[0];

        $playerEvent = new PlayerEvent();
        $playerEvent->setPlayer($player);
        $playerEvent->setEvent($event);

        $match = $em->getRepository(PlayerEvent::class)->findOneBy([
            'player' => $playerEvent->getPlayer(),
            'event' => $playerEvent->getEvent(),
        ]);

        $playerEvent->setIsPrimary(is_null($match));

        $em->persist($playerEvent);
        $em->flush();

        return $playerEvent;
    }

    /**
     * @param Player $player
     * @param array $codes
     * @return array
     * @throws \Exception
     */
    public function createBatch(Player $player, array $codes)
    {
        $em = $this->container->get('doctrine')->getManager();

        $events = $em->getRepository(Event::class)->findByFilter([
            'code' => $codes,
            'locale' => $player->getLocale()->getCode()
        ]);
        if (count($events) === 0) {
            throw new \Exception('Event was not found', 404);
        }

        $playerEvents = [];

        /** @var Event $event */
        foreach ($events as $event) {

            $match = $em->getRepository(PlayerEvent::class)->findOneBy([
                'player' => $player,
                'event' => $event,
            ]);

            $playerEvent = new PlayerEvent();
            $playerEvent->setPlayer($player);
            $playerEvent->setEvent($event);
            $playerEvent->setIsPrimary(is_null($match));

            $em->persist($playerEvent);

            $playerEvents[] = $playerEvent;
        }

        if (count($playerEvents) > 0) {
            $em->flush();
        }

        return $playerEvents;
    }

    /**
     * @param Player $player
     * @param array $filter
     * @return int
     * @throws \Exception
     */
    public function countByFilter(Player $player, array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        $filter['players'] = [$player->getId()];
        $filter['isPrimary'] = true;

        return $em->getRepository(PlayerEvent::class)->countByFilter($filter);
    }

    /**
     * @param Player $player
     * @param array $filter
     * @return array
     * @throws \Exception
     */
    public function findByFilter(Player $player, array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        $filter['players'] = [$player->getId()];
        $filter['isPrimary'] = true;

        return $em->getRepository(PlayerEvent::class)->findByFilter($filter);
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}