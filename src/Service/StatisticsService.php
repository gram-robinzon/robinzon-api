<?php

namespace App\Service;

use App\Entity\Event;
use App\Entity\PlayerEvent;
use App\Entity\Statistics;
use Doctrine\DBAL\Connection;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StatisticsService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Statistics::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     *
     * @return array
     * @throws \Exception
     */
    public function findByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Statistics::class)->findByFilter($filter);
    }

    /**
     * @throws \Exception
     */
    public function forceUpdate()
    {
        $em = $this->container->get('doctrine')->getManager();

        $events = $em->getRepository(Event::class)->findAll();

        /** @var Event $event */
        foreach ($events as $event) {
            $count = $this->countByFilter([
                'event' => $event->getId()
            ]);
            if ($count === 0) {
                $statistics = new Statistics();
                $statistics->setEvent($event);
                $statistics->setCategory($event->getCategory());
                $statistics->setPercent(0);

                $em->persist($statistics);
            }
        }

        $em->flush();

        $playerEvents = $em->getRepository(PlayerEvent::class)->findAll();

        $categoryRegistry = [];
        $categoryEventRegistry = [];

        /** @var PlayerEvent $playerEvent */
        foreach ($playerEvents as $playerEvent) {

            $categoryId = $playerEvent->getEvent()->getCategory()->getId();

            if (!isset($categoryEventRegistry[$categoryId])) {
                $categoryEventRegistry[$categoryId] = [];
            }

            $categoryEventRegistry[$categoryId][] = $playerEvent;
            $categoryRegistry[$categoryId][] = $playerEvent->getEvent()->getCategory();
        }

        /** @var Connection $connection */
        $connection = $em->getConnection();

        $connection->beginTransaction();

        try {

            foreach ($categoryEventRegistry as $categoryId => $playerEvents) {

                $events = $em->getRepository(Event::class)->findBy([
                    'category' => $categoryId
                ]);

                $eventCountRegistry = [];
                /** @var Event $event */
                foreach ($events as $event) {
                    $eventCountRegistry[$event->getCode()] = 0;
                }

                $total = 0;
                /** @var PlayerEvent $playerEvent */
                foreach ($playerEvents as $playerEvent) {
                    $code = $playerEvent->getEvent()->getCode();

                    ++$eventCountRegistry[$code];
                    ++$total;
                }

                foreach ($events as $event) {

                    $percent = 0;
                    $value = $eventCountRegistry[$event->getCode()];
                    if ($total > 0) {
                        $percent = 100 * $value / $total;
                    }

                    $connection->prepare('UPDATE statistics SET percent = '
                        . round($percent, 2)
                        . ' WHERE event_id = '
                        . $event->getId()
                    )->execute();
                }
            }

            $connection->commit();

        } catch (\Exception $e) {

            if ($connection->isTransactionActive()) {
                $connection->rollBack();
            }

            throw $e;
        }
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }
}