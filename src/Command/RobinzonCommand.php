<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class RobinzonCommand extends ContainerAwareCommand
{
    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->executeWithMonitoring($input, $output);

            $this->onComplete();

        } catch (\Exception $e) {
            $this->onError($e);

            throw $e;
        }
    }

    protected abstract function executeWithMonitoring(InputInterface $input, OutputInterface $output);

    protected function onComplete()
    {
        $message = sprintf("`%s` OK", $this->getName());
        $this->notifySuccessSlack($message);
    }

    protected function onError(\Exception $e)
    {
        $traceLine = '';

        foreach ($e->getTrace() as $trace) {
            if (isset($trace['file']) && isset($trace['line'])) {
                $traceLine .= $trace['file'] . "::" . $trace['line'] . "\n";
            }
        }

        $message = sprintf("*From*: `%s`\n*Content*: %s\n*Code*: %s\n*File*:\n%s::%s\n*Trace*:\n%s",
            $this->getName(),
            $e->getMessage(),
            $e->getCode(),
            $e->getFile(),
            $e->getLine(),
            $traceLine);

        $this->notifyErrorSlack($message);
    }

    private function notifyErrorSlack($message)
    {
        $accessToken = $this->getContainer()->getParameter('slack_error_webhook');
        $this->notifySlack($message, $accessToken);
    }

    private function notifySuccessSlack($message)
    {
        $accessToken = $this->getContainer()->getParameter('slack_success_webhook');
        $this->notifySlack($message, $accessToken);
    }

    private function notifySlack($message, $accessToken)
    {
        if (!$this->isProd()) return;

        $ch = curl_init('https://hooks.slack.com/services/' . $accessToken);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'payload' => '{"text": "' . $message . '"}'
        ));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_exec($ch);

        curl_close($ch);
    }

    protected function isProd()
    {
        $env = $this->getContainer()->getParameter('kernel.environment');
        return $env === 'prod';
    }

}