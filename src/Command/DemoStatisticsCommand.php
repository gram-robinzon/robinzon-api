<?php

namespace App\Command;

use App\Entity\Category;
use App\Entity\Event;
use App\Entity\Locale;
use App\Service\PlayerEventService;
use App\Service\PlayerService;
use App\Service\StatisticsService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DemoStatisticsCommand extends RobinzonCommand
{
    const NAME = 'robinzon:stats:demo';

    protected function configure()
    {
        $this->setName(self::NAME);
    }

    protected function executeWithMonitoring(InputInterface $input, OutputInterface $output)
    {
        $localeCode = Locale::UK;
        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Started');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $playserService = $this->getContainer()->get(PlayerService::class);
        $statService = $this->getContainer()->get(StatisticsService::class);
        $playerEventService = $this->getContainer()->get(PlayerEventService::class);

        $categories = $em->getRepository(Category::class)->findByFilter([
            'locale' => $localeCode
        ]);

        try {
            for ($i = 0; $i < 15; $i++) {
                $device = md5(uniqid($i));
                $name = "test_" . md5(uniqid($i));
                $player = $playserService->findOrCreate($device, $name, $localeCode);

                $codes = [];

                $output->writeln('[+] Player ' . $name);

                /** @var Category $category */
                foreach ($categories as $category) {
                    $eventSize = $category->getEvents()->count();
                    /** @var Event $event */
                    $event = $category->getEvents()->get(rand(0, $eventSize - 1));

                    $codes[] = $event->getCode();

                    $output->writeln('[+] => ' . $event->getCode());
                }

                $playerEventService->createBatch($player, $codes);
            }
        } catch (\Throwable $e) {
            $output->writeln('[-] ' . $e->getMessage());
            exit(1);
        }

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Updating statistics...');

        $statService->forceUpdate();

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Finished');
    }
}