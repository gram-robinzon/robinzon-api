<?php

namespace App\Command;

use App\Entity\Expansion;
use App\Entity\Locale;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExpansionSizeCommand extends RobinzonCommand
{
    const NAME = 'robinzon:expansions:size';

    protected function configure()
    {
        $this->setName(self::NAME)
            ->addArgument('version', InputArgument::REQUIRED, 'Version of the migration')
            ->addArgument('locale', InputArgument::REQUIRED, 'Locale of the migration');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function executeWithMonitoring(InputInterface $input, OutputInterface $output)
    {
        $version = $input->getArgument('version');
        if (!$version) {
            throw new \Exception("Missing version", 500);
        }

        $localeCode = $input->getArgument('locale');
        if (!$localeCode) {
            throw new \Exception("Missing locale", 500);
        }

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Started');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $expansions = $em->getRepository(Expansion::class)->findBy([
            'version' => $version,
            'locale' => $localeCode
        ], [
            'dpi' => "DESC",
            'device' => "DESC",
        ]);
        if (!$expansions) {
            throw new \Exception("Expansions were not found", 404);
        }

        $dir = $this->getContainer()->getParameter('expansions_dir') . '/..';

        /** @var Expansion $expansion */
        foreach ($expansions as $expansion) {

            $file = $dir . $expansion->getFile();

            if (!file_exists($file)) {
                throw new \Exception("Expansion was not found at " . $expansion->getFile(), 404);
            }

            $size = filesize($file);

            $output->writeln("[$size] " . $expansion->getFile());

            if ($size === 0) {
                throw new \Exception("Invalid filesize for expansion " . $expansion->getFile(), 500);
            }

            $expansion->setSize($size);

            $em->persist($expansion);
        }

        $em->flush();

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Finished');
    }
}