<?php

namespace App\Command;

use App\Service\StatisticsService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StatisticsCommand extends RobinzonCommand
{
    const NAME = 'robinzon:stats';

    protected function configure()
    {
        $this->setName(self::NAME);
    }

    protected function executeWithMonitoring(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Started');
        $this->getContainer()->get(StatisticsService::class)->forceUpdate();
        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Finished');
    }
}