<?php

namespace App\Command;

use App\Entity\Expansion;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExpansionGenerateCommand extends RobinzonCommand
{
    const NAME = 'robinzon:expansions:generate';

    protected function configure()
    {
        $this->setName(self::NAME)
            ->addArgument('version', InputArgument::REQUIRED, 'Version of the migration');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function executeWithMonitoring(InputInterface $input, OutputInterface $output)
    {
        $version = $input->getArgument('version');
        if (!$version) {
            throw new \Exception("Missing version", 500);
        }

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Started');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $match = $em->getRepository(Expansion::class)->findOneBy([
            'version' => $version
        ]);
        if ($match) {
            throw new \Exception("Expansion with version $version already exists", 500);
        }

        /** @var Connection $connection */
        $connection = $em->getConnection();

        foreach (['uk', 'ru', 'en'] as $locale) {
            foreach (['large', 'xlarge', 'normal'] as $device) {
                foreach (['mdpi', 'hdpi', 'xhdpi', 'xxhdpi', 'xxxhdpi'] as $dpi) {
                    $connection->prepare("INSERT INTO expansion (locale, dpi, device, version, file)
                    VALUES ('$locale', '$dpi', '$device', $version, '/expansions/$locale/$dpi/$device/$version-expansion.obb')")
                        ->execute();
                }
            }
        }

        $output->writeln('[+] ' . date('Y-m-d H:i:s') . ' Finished');
    }
}