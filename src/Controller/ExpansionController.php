<?php

namespace App\Controller;

use App\Classes\Expansion;
use App\Service\ExpansionService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ExpansionController extends Controller
{

    /**
     * Get information about expansion file, which is best-suited for smartphone descibed by query parameters.
     *
     * @param $locale
     * @param $device
     * @param $dpi
     * @param $version
     * @return JsonResponse
     */
    public function getsAction($locale, $device, $dpi, $version)
    {
        $expansionService = $this->get(ExpansionService::class);
        try {

            $expansion = $expansionService->findOneByFilter([
                'locale' => $locale,
                'device' => $device,
                'dpi' => $dpi,
                'version' => $version,
            ]);
            if (!$expansion) {
                throw new \Exception('Not found', Response::HTTP_NOT_FOUND);
            }

            $item = $expansionService->serialize($expansion);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}