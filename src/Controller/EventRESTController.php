<?php

namespace App\Controller;

use App\Service\EventService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventRESTController extends Controller
{

    /**
     * Get information about important events
     *
     * @param $locale
     * @return JsonResponse
     */
    public function getsAction($locale)
    {
        $service = $this->get(EventService::class);

        $filter = [
            'locale' => $locale
        ];

        try {

            $items = [];
            $total = $service->countByFilter($filter);
            if ($total > 0) {

                $entities = $service->findByFilter($filter);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'total' => $total,
                'count' => count($items),
                'items' => $items
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}