<?php

namespace App\Controller;

use App\Service\CategoryService;
use App\Service\PlayerEventService;
use App\Service\PlayerService;
use App\Service\StatisticsService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class PlayerStatisticsRESTController extends Controller
{

    /**
     * Get total statistics with infromation about specific player
     *
     * @param $locale
     * @param $name
     * @param $device
     * @return JsonResponse
     */
    public function getAction($locale, $name, $device)
    {
        $playerService = $this->get(PlayerService::class);
        $playerEventService = $this->get(PlayerEventService::class);
        $statService = $this->get(StatisticsService::class);
        $categoryService = $this->get(CategoryService::class);

        $filter = [
//            'locale' => $locale
        ];

        $categoryFilter = [
            'locale' => $locale
        ];

        try {
            $player = $playerService->findOrCreate($device, $name, $locale);

            $items = [];
            $total = $statService->countByFilter($filter);
            if ($total > 0) {

                $entities = $statService->findByFilter($filter);

                $items = $statService->serialize($entities);
            }

            $playerItems = [];
            $totalEvents = $playerEventService->countByFilter($player, $filter);
            if ($totalEvents > 0) {

                $entities = $playerEventService->findByFilter($player, $filter);

                $playerItems = $playerEventService->serialize($entities);
            }

            $categoryItems = [];
            $totalCategories = $categoryService->countByFilter($categoryFilter);
            if ($totalCategories > 0) {

                $entities = $categoryService->findByFilter($categoryFilter);

                $categoryItems = $categoryService->serialize($entities);
            }

            return new JsonResponse([
                'requestedAt' => date('Y-m-d H:i:s'),
                'categories' => [
                    'total' => $totalCategories,
                    'count' => count($categoryItems),
                    'items' => $categoryItems
                ],
                'statistics' => [
                    'total' => $total,
                    'count' => count($items),
                    'items' => $items
                ],
                'playerEvents' => [
                    'total' => $totalEvents,
                    'count' => count($playerItems),
                    'items' => $playerItems
                ]
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get total statistics
     *
     * @param $locale
     * @return JsonResponse
     */
    public function getsAction($locale)
    {
        $statService = $this->get(StatisticsService::class);
        $categoryService = $this->get(CategoryService::class);

        $filter = [
            'locale' => $locale
        ];

        $categoryFilter = [
            'locale' => $locale
        ];

        try {

            $items = [];
            $total = $statService->countByFilter($filter);
            if ($total > 0) {

                $entities = $statService->findByFilter($filter);

                $items = $statService->serialize($entities);
            }

            $categoryItems = [];
            $totalCategories = $categoryService->countByFilter($categoryFilter);
            if ($totalCategories > 0) {

                $entities = $categoryService->findByFilter($categoryFilter);

                $categoryItems = $categoryService->serialize($entities);
            }

            return new JsonResponse([
                'requestedAt' => date('Y-m-d H:i:s'),
                'categories' => [
                    'total' => $totalCategories,
                    'count' => count($categoryItems),
                    'items' => $categoryItems
                ],
                'statistics' => [
                    'total' => $total,
                    'count' => count($items),
                    'items' => $items
                ]
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}