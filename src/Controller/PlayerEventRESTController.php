<?php

namespace App\Controller;

use App\Service\PlayerEventService;
use App\Service\PlayerService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PlayerEventRESTController extends Controller
{

    /**
     * Save single event for player
     *
     * @param $locale
     * @param $name
     * @param $device
     * @param $code
     * @return JsonResponse
     */
    public function postAction($locale, $name, $device, $code)
    {
        $playerService = $this->get(PlayerService::class);
        $playerEventService = $this->get(PlayerEventService::class);

        try {
            $player = $playerService->findOrCreate($device, $name, $locale);

            $playerEvent = $playerEventService->create($player, $code);

            $item = $playerEventService->serialize($playerEvent);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Save events batch for single player
     *
     * @param Request $request
     * @param $locale
     * @param $name
     * @param $device
     * @return JsonResponse
     */
    public function postBatchAction(Request $request, $locale, $name, $device)
    {
        $content = json_decode($request->getContent(), true);
        $codes = [];

        try {
            if (!$content) {
                throw new \Exception('Invalid content', JsonResponse::HTTP_BAD_REQUEST);
            }

            foreach ($content as $event) {
                if (!isset($event['code'])) {
                    throw new \Exception('Missing code', JsonResponse::HTTP_BAD_REQUEST);
                }

                $codes[$event['code']] = $event['code'];
            }

            if (count($codes) === 0) {
                throw new \Exception('Missing content', JsonResponse::HTTP_BAD_REQUEST);
            }

            $codes = array_values($codes);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        $playerService = $this->get(PlayerService::class);
        $playerEventService = $this->get(PlayerEventService::class);

        try {
            $player = $playerService->findOrCreate($device, $name, $locale);

            $playerEvents = $playerEventService->createBatch($player, $codes);
            if (count($playerEvents) === 0) {
                return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
            }

            $items = $playerEventService->serialize($playerEvents);

            return new JsonResponse($items, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Clear events for specific player
     *
     * @param $locale
     * @param $name
     * @param $device
     * @return JsonResponse
     */
    public function deleteAction($locale, $name, $device)
    {
        $playerService = $this->get(PlayerService::class);
        $playerEventService = $this->get(PlayerEventService::class);

        try {
            $player = $playerService->find($device, $name, $locale);

            $playerEventService->remove($player);

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}