<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExpansionRepository")
 * @ORM\Table
 */
class Expansion
{

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="bigint")
     *
     * @JMS\Groups({"api_v1"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, unique=true)
     *
     * @JMS\Groups({"api_v1"})
     */
    private $file;

    /**
     * @var int
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @JMS\Groups({"api_v1"})
     */
    private $locale;

    /**
     * @var int
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @JMS\Groups({"api_v1"})
     */
    private $dpi;

    /**
     * @var int
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @JMS\Groups({"api_v1"})
     */
    private $device;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     *
     * @JMS\Groups({"api_v1"})
     */
    private $version;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=false)
     *
     * @JMS\Groups({"api_v1"})
     */
    private $size;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(string $file): void
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getLocale(): ?int
    {
        return $this->locale;
    }

    /**
     * @param int $locale
     */
    public function setLocale(int $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function getDpi(): ?int
    {
        return $this->dpi;
    }

    /**
     * @param int $dpi
     */
    public function setDpi(int $dpi): void
    {
        $this->dpi = $dpi;
    }

    /**
     * @return int
     */
    public function getDevice(): ?int
    {
        return $this->device;
    }

    /**
     * @param int $device
     */
    public function setDevice(int $device): void
    {
        $this->device = $device;
    }

    /**
     * @return int
     */
    public function getVersion(): ?int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }
}