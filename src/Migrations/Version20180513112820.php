<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Category;
use App\Entity\Locale;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180513112820 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        foreach ([Locale::RU, Locale::EN, Locale::UK] as $locale) {
            for ($i = 0; $i < 18; $i++) {

                $type = $i === 10 || $i === 3
                    ? Category::INTERACTION
                    : Category::BRANCH;

                $this->addSql("INSERT INTO category (ordering, type, locale_id) VALUES ($i, '$type', (SELECT id FROM locale WHERE code = '$locale'))");
            }
        }

    }

    public function down(Schema $schema): void
    {

    }
}
