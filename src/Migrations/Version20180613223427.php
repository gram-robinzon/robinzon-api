<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180613223427 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE expansion (id BIGSERIAL NOT NULL, size BIGINT NOT NULL DEFAULT 0, file VARCHAR(255) NOT NULL, locale VARCHAR(255) NOT NULL, version INT NOT NULL, dpi VARCHAR(255) NOT NULL, device VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F0695B728C9F3610 ON expansion (file)');
    }

    public function down(Schema $schema) : void
    {
    }
}
