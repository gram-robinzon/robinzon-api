<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Locale;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180513112823 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a1a', 'Коли пісок засліпив тобі очі, в першу чергу ти і __X__ закричали', 'p1a', (SELECT id FROM category WHERE ordering = 0 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a1b', 'Коли пісок засліпив тобі очі, в першу чергу ти і __X__ одразу протерли очі', 'p1b', (SELECT id FROM category WHERE ordering = 0 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a2dba', 'Ти і __X__ слухачів вибрали парашут і покинули пілота напризволяще', 'p2dba', (SELECT id FROM category WHERE ordering = 1 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a2dbb', 'Ти і __X__ героїв не кинули діда-пілота у скруті, а замінили його і посадили палаючу крихітку (в море)', 'p2dbb', (SELECT id FROM category WHERE ordering = 1 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b1', 'Ти і __X__ збили камінцем кокос з першого разу, кинувши камінь під вирахуваним кутом', 'p4b1', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b2', 'Ти і __X__ збили камінцем кокос з другого разу, кинувши камінь під вирахуваним кутом', 'p4b2', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b3', 'Ти і __X__ збили камінцем кокос з третього разу, кинувши камінь під вирахуваним кутом', 'p4b3', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b4', 'Ти і __X__ збили камінцем кокос з останнього разу, кинувши камінь під вирахуваним кутом', 'p4b4', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a8a', 'Ти і __X__ пішли шукати собі пригод на останній гілці', 'p8a', (SELECT id FROM category WHERE ordering = 3 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a8b', 'Ти і __X__ залишились без пригод на останній гілці', 'p8b', (SELECT id FROM category WHERE ordering = 3 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a12a', 'Ти і __X__ рушили ліворуч по стежці', 'p12a', (SELECT id FROM category WHERE ordering = 4 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a12b', 'Ти і __X__ рушили праворуч по стежці', 'p12b', (SELECT id FROM category WHERE ordering = 4 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14a', 'Ти і __X__ людей назвали кеш’ю “Авокадом”', 'p14a', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14b', 'Ти і __X__ людей назвали кеш’ю “Мангом”', 'p14b', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14c', 'Ти і __X__ людей назвали кеш’ю “Кеш’ю”', 'p14c', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14d', 'Ти і __X__ диких людей назвали дике кеш’ю “Диким яблуком”', 'p14d', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15a', 'Ти і __X__ робінзонів поселилось у комфортній халупці', 'p15a', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15b', 'Ти і __X__ робінзонів вибрали шлях індіанців, збудувавши конічний вігвам', 'p15b', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15c', 'Ти і __X__ робінзонів ночувало в затишній землянці', 'p15c', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15d', 'Ти і __X__ робінзонів ночувало під відкритим небом, обличчям у багнюці', 'p15d', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16a', 'Ти і __X__ слухачів поласували белладонною. Дуже смачно. А потім вмерли.', 'p16a', (SELECT id FROM category WHERE ordering = 7 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16b', 'Ти і __X__ слухачів обійшли стороною підозрілий кущ. І не прогадали!', 'p16b', (SELECT id FROM category WHERE ordering = 7 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16aa', 'Ти і __X__ отруєних гурманів побачили НЛО в морі! І чорний промінь осліпив їх!', 'p16aa', (SELECT id FROM category WHERE ordering = 8 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16ab', 'Ти і __X__ отруєних гурманів побігли геть від негрів-піратів!', 'p16ab', (SELECT id FROM category WHERE ordering = 8 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16ac', 'Ти і __X__ отруєних гурманів було піймано податківцями, від яких ти ховався все своє життя!', 'p16ac', (SELECT id FROM category WHERE ordering = 8 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20a', 'Ти і __X__ робінзонів, щоб добути вогонь, пішли за фруктами', 'p20a', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20b', 'Ти і __X__ робінзонів, щоб добути вогонь,скористалися поліетиленовим пакетиком', 'p20b', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20c', 'Ти і __X__ робінзонів добули вогонь іскрою з пірита', 'p20c', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20e', 'Ти і __X__ робінзонів відправились за вогнем на Олімп!', 'p20e', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a21a', 'Ти і __X__ переможців кинулись до вогню, у пошуках захисту від пуми', 'p21a', (SELECT id FROM category WHERE ordering = 10 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a21b', 'Ти і __X__ бідолах перелякалось, не намацавши кинджалу, побачивши пуму', 'p21b', (SELECT id FROM category WHERE ordering = 10 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a21c', 'Ти і __X__ бідолах почали кивати п’ятами від пуми, але не докивали', 'p21c', (SELECT id FROM category WHERE ordering = 10 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23a', 'Ти і __X__ робінзонів спробувало викопати пастку-яму, щоб перемогти тварину, але невдало!', 'p23a', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23b', 'Ти і __X__ робінзонів підточили кинджал, щоб зустрітися зі звіром!', 'p23b', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23c', 'Ти і __X__ робінзонів заточили палку і назвали це списом. О, жахайся, пума, заточеної палки!', 'p23c', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23d', 'Ти і __X__ робінзонів винайшли лук для захисту від пуми.', 'p23d', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23e', 'Ти і __X__ хитрунів отруїли кинджал.', 'p23e', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a29faa', 'Ти і __X__ везунчиків побили пуму кулаками.', 'p29fab', (SELECT id FROM category WHERE ordering = 12 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a29fab', 'Ти і __X__ невдах полізли до пуми з кулаками і отримали прочухана. Сильного прочухана!', 'p29fab', (SELECT id FROM category WHERE ordering = 12 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32a', '__X__ робінзонів і Ти поплили геть з цього острову. На плоту.', 'p32a', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32b', '__X__ робінзонів і Ти нарубали дров, поприносили деревини - для сигнального багаття!', 'p32b', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32c', '__X__ робінзонів і Ти підпалили острів - хай горить собі. А шо?', 'p32c', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32d', '__X__ робінзонів і Ти виклали на березі улюблене слово, аби хто-небудь побачив!', 'p32d', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32e', '__X__ робінзонів і Ти вирішили запустити сигнальну ракету!', 'p32e', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32f', '__X__ робінзонів і Ти вирішили подивитися на мапу ще раз.', 'p32f', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32g', '__X__ робінзонів і Ти вирішили збудувати маяк, аби привернути увагу!', 'p32g', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32da', 'Ти і __X__ слововикладачів на березі вибрали слово “SOS”', 'p32da', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32db', 'Ти і __X__ слововикладачів на березі вибрали слово “Мама”', 'p32db', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32dc', 'Ти і __X__ слововикладачів на березі вибрали слово “Help”', 'p32dc', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32dd', 'Ти і __X__ слововикладачів на березі вибрали слово “Памагите”', 'p32dd', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontska', 'Ти і __X__ робінзонів не обміняли свою шкіру пуми ні на яке багатство!', 'p37ccontska', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontskb', 'Ти і __X__ робінзонів обернули шкіру пуми на мільйон', 'p37ccontskb', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontskc', 'Ти і __X__ робінзонів отримали титул “Король острову” (“Віслюк”)', 'p37ccontskc', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontskd', 'Ти і __X__ робінзонів махнули шкіру пуми на золото, діамант сто каратів і 1000$', 'p37ccontskd', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a33a', 'Ти і __X__ матросів заспівали веселу пісеньку у морі', 'p33a', (SELECT id FROM category WHERE ordering = 16 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a33b', 'Ти і __X__ матросів засумували і заспівали сумну пісню у морі', 'p33b', (SELECT id FROM category WHERE ordering = 16 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::UK . "')))");

    }

    public function down(Schema $schema): void
    {

    }
}
