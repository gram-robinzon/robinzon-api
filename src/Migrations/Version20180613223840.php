<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180613223840 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $version = 3;
        foreach (['uk', 'ru', 'en'] as $locale) {
            foreach (['large', 'xlarge', 'normal'] as $device) {
                foreach (['mdpi', 'hdpi', 'xhdpi', 'xxhdpi', 'xxxhdpi'] as $dpi) {
                    $this->addSql("INSERT INTO expansion (locale, dpi, device, version, file)
                    VALUES ('$locale', '$dpi', '$device', $version, '/expansions/$locale/$dpi/$device/$version-expansion.obb')");
                }
            }
        }

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
