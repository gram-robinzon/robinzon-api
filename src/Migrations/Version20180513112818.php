<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Locale;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180513112818 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO locale (code) VALUES ('" . Locale::EN . "'),('" . Locale::RU . "'),('" . Locale::UK . "')");
    }

    public function down(Schema $schema): void
    {

    }
}
