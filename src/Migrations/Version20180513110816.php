<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180513110816 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE event (id BIGSERIAL NOT NULL, category_id BIGINT NOT NULL, code VARCHAR(12) NOT NULL, name TEXT NOT NULL, icon VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3BAE0AA777153098 ON event (code)');
        $this->addSql('CREATE INDEX IDX_3BAE0AA712469DE2 ON event (category_id)');
        $this->addSql('CREATE TABLE statistics (id BIGSERIAL NOT NULL, category_id BIGINT NOT NULL, event_id BIGINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, percent DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E2D38B2212469DE2 ON statistics (category_id)');
        $this->addSql('CREATE INDEX IDX_E2D38B2271F7E88B ON statistics (event_id)');
        $this->addSql('CREATE TABLE category (id BIGSERIAL NOT NULL, locale_id BIGINT NOT NULL, ordering SMALLINT NOT NULL, type VARCHAR(16) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_64C19C1E559DFD1 ON category (locale_id)');
        $this->addSql('CREATE TABLE player_event (id BIGSERIAL NOT NULL, player_id BIGINT NOT NULL, event_id BIGINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_primary BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_84DC71E199E6F5DF ON player_event (player_id)');
        $this->addSql('CREATE INDEX IDX_84DC71E171F7E88B ON player_event (event_id)');
        $this->addSql('CREATE TABLE locale (id BIGSERIAL NOT NULL, code VARCHAR(10) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4180C69877153098 ON locale (code)');
        $this->addSql('CREATE TABLE player (id BIGSERIAL NOT NULL, locale_id BIGINT NOT NULL, name VARCHAR(255) NOT NULL, device VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_98197A65E559DFD1 ON player (locale_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_98197A6592FB68E5E237E06 ON player (device, name)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA712469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B2212469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B2271F7E88B FOREIGN KEY (event_id) REFERENCES event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C1E559DFD1 FOREIGN KEY (locale_id) REFERENCES locale (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player_event ADD CONSTRAINT FK_84DC71E199E6F5DF FOREIGN KEY (player_id) REFERENCES player (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player_event ADD CONSTRAINT FK_84DC71E171F7E88B FOREIGN KEY (event_id) REFERENCES event (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE player ADD CONSTRAINT FK_98197A65E559DFD1 FOREIGN KEY (locale_id) REFERENCES locale (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema) : void
    {

    }
}
