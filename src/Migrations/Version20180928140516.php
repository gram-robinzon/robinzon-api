<?php declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Category;
use App\Entity\Locale;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180928140516 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO category (ordering, type, locale_id) VALUES (18, '" . Category::BRANCH . "', (SELECT id FROM locale WHERE code = '" . Locale::RU . "'))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a1a', 'Когда песок заслепил тебе глаза, ты и __Х__ в первую очередь закричали', 'p1a', (SELECT id FROM category WHERE ordering = 0 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a1b', 'Когда песок заслепил тебе глаза, ты и __Х__ в первую очередь протёрли глаза', 'p1b', (SELECT id FROM category WHERE ordering = 0 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a2dba', 'Ты и __Х__ слушателей выбрали парашют и бросили пилота на произвол судьбы', 'p2dba', (SELECT id FROM category WHERE ordering = 1 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a2dbb', 'Ты и __Х__ героев не бросили деда-пилота в беде, а сменили его за штурвалом и посадили горящую крошку в море', 'p2dbb', (SELECT id FROM category WHERE ordering = 1 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b1', 'Ты и __Х__ сбили камнем кокос с первого! раза, бросив камень под высчитаным углом', 'p4b1', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b2', 'Ты и __Х__ сбили камнем кокос со второго раза, бросив камень под высчитаным углом', 'p4b2', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b3', 'Ты и __Х__ сбили камнем кокос с третьего раза, бросив камень под высчитаным углом', 'p4b3', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a4b4', 'Ты и __Х__ сбили камнем кокос с последнего раза, бросив камень под высчитаным углом', 'p4b4', (SELECT id FROM category WHERE ordering = 2 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a8a', 'Ты и __Х__ пошли искать себе приключений на последней ветке', 'p8a', (SELECT id FROM category WHERE ordering = 3 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a8b', 'Ты и __Х__ остались без приключений на последней ветке', 'p8b', (SELECT id FROM category WHERE ordering = 3 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a12a', 'Ты и __Х__ двинулись налево по тропе', 'p12a', (SELECT id FROM category WHERE ordering = 4 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a12b', 'Ты и __Х__ двинулись направо по тропе', 'p12b', (SELECT id FROM category WHERE ordering = 4 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14a', 'Ты и __Х__ людей назвали кешью \"Авокадом\"', 'p14a', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14b', 'Ты и __Х__ людей назвали кешью \"Мангом\"', 'p14b', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14c', 'Ты и __Х__ людей назвали кешью \"Кешью\"', 'p14c', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a14d', 'Ты и __Х__ диких людей назвали кешью \"Диким яблоком\"', 'p14d', (SELECT id FROM category WHERE ordering = 5 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15a', 'Ты и __Х__ робинзонов поселились в комфортной хижине ', 'p15a', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15b', 'Ты и __Х__ робинзонов выбрали путь индейцев, построив конический вигвам', 'p15b', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15c', 'Ты и __Х__ робинзонов ночевали в уютной землянке', 'p15c', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a15d', 'Ты и __Х__ робинзонов ночевало под открытым небом, лицом в грязи', 'p15d', (SELECT id FROM category WHERE ordering = 6 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16a', 'Ты и __Х__ слушателей полакомились белладонной. Очень вкусно. А потом отравились', 'p16a', (SELECT id FROM category WHERE ordering = 7 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16b', 'Ты и __Х__ обошли подозрительный куст стороною. И не прогадали!', 'p16b', (SELECT id FROM category WHERE ordering = 7 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16aa', 'Ты и __Х__ отравившихся гурманов увидели НЛО в море! И чёрный луч ослепил им глаза!', 'p16aa', (SELECT id FROM category WHERE ordering = 8 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16ab', 'Ты и __Х__ отравившихся гурманов побежали прочь от негров-пиратов!', 'p16ab', (SELECT id FROM category WHERE ordering = 8 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a16ac', 'Ты и __Х__ отравившихся гурманов были пойманы налоговиками, от которых прятались всю свою жизнь!', 'p16ac', (SELECT id FROM category WHERE ordering = 8 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20a', 'Ты и __Х__ робинзонов, что бы добыть огонь, пошли за фруктами', 'p20a', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20b', 'Ты и __Х__ робинзонов, что бы добыть огонь, использовали полиэтиленовый пакетик', 'p20b', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20c', 'Ты и __Х__ робинзонов добыли огонь из пирита', 'p20c', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a20e', 'Ты и __Х__ робинзонов отправились за огнём на Олимп!', 'p20e', (SELECT id FROM category WHERE ordering = 9 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a21a', 'Ты и __Х__ победителей кинулись к огню в поисках спасения от пумы', 'p21a', (SELECT id FROM category WHERE ordering = 10 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a21b', 'Ты и __Х__ бедняг испугались, не нащупав кинжала рядом, увидев пуму', 'p21b', (SELECT id FROM category WHERE ordering = 10 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a21c', 'Ты и __Х__ бедняг  накивали пятками, увидев пуму, но не докивали', 'p21c', (SELECT id FROM category WHERE ordering = 10 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23a', 'Ты и __Х__ робинзонов попробывали выкопать яму-ловушку, что бы победить животное, но неудачно!', 'p23a', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23b', 'Ты и __Х__ робинзонов подточили кинжал, что бы встретиться с зверем!', 'p23b', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23c', 'Ты и __Х__ робинзонов заточили палку и назвали это копьем. О! Бойся, пума, заточеной палки!', 'p23c', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23d', 'Ты и __Х__ робинзонов изобрели лук для защиты от пумы', 'p23d', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a23e', 'Ты и __Х__ хитрецов отравили кинжал', 'p23e', (SELECT id FROM category WHERE ordering = 11 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a29faa', 'Ты и __Х__ везунчиков проучили пуму тумаками', 'p29fab', (SELECT id FROM category WHERE ordering = 12 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a29fab', 'Ты и __Х__ неудачников попёрли на пуму с кулаками и получили нагоняя! Сильного нагоняя!', 'p29fab', (SELECT id FROM category WHERE ordering = 12 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32a', '__Х__ робинзонов и Ты уплыли прочь с этого острова. На плоту', 'p32a', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32b', '__Х__ робинзонов и Ты нарубали дров, поприносили древесины - для сигнального костра!', 'p32b', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32c', '__Х__ робинзонов и Ты подпалили остров - пусть себе горит. А шо?', 'p32c', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32d', '__Х__ робинзонов и Ты выложили на берегу любимое слово, лишь бы хто увидел!', 'p32d', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32e', '__Х__ робинзонов и Ты решили запустить сигнальную ракету!', 'p32e', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32f', '__Х__ робинзонов и Ты решили посмотреть на карту еще разок', 'p32f', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32g', '__Х__ робинзонов и Ты решили построить маяк, лишь бы привлечь внимание!', 'p32g', (SELECT id FROM category WHERE ordering = 13 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32da', 'Ты и __Х__ словосоздателей выложили на берегу слово \"SOS\"', 'p32da', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32db', 'Ты и __Х__ словосоздателей выложили на берегу слово \"МАМА\"', 'p32db', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32dc', 'Ты и __Х__ словосоздателей выложили на берегу слово \"HELP\"', 'p32dc', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a32dd', 'Ты и __Х__ словосоздателей выложили на берегу слово \"ПАМАГИТЕ\"', 'p32dd', (SELECT id FROM category WHERE ordering = 14 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontska', 'Ты и __Х__ робинзонов не променяли свою шкуру пуму ни на какое багатство!', 'p37ccontska', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontskb', 'Ты и __Х__ робинзонов разменяли шкуру пумы на милион', 'p37ccontskb', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontskc', 'Ты и __Х__ робинзонов получили титул \"Король острова\" (\"Осёл\")', 'p37ccontskc', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a37ccontskd', 'Ты и __Х__ робинзонов разменяли шкуру пумы на золото, диамант сто каратов и 1000$', 'p37ccontskd', (SELECT id FROM category WHERE ordering = 15 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a33a', 'Ты и __Х__ матросов затянули весёлую песню в море', 'p33a', (SELECT id FROM category WHERE ordering = 16 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a33b', 'Ты и __Х__ матросов загрустили и запели грустную песню', 'p33b', (SELECT id FROM category WHERE ordering = 16 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a1a_bonusa', 'Ты и __Х__ слушателей не воспользовались спасательной услугой, попав на остров. Они хотели приключений!', 'p_bonus_a', (SELECT id FROM category WHERE ordering = 18 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");
        $this->addSql("INSERT INTO event (code, name, icon, category_id) VALUES ('a1a_bonusb', 'Ты и __Х__ слушателей чуть было не расплакались, когда спасатель уносил их на плечах прочь с острова!', 'p_bonus_b', (SELECT id FROM category WHERE ordering = 18 AND locale_id = (SELECT id FROM locale WHERE code = '" . Locale::RU . "')))");

    }

    public function down(Schema $schema): void
    {

    }
}
