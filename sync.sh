#!/bin/bash

echo '[+] Started sync with remote server...'

rsync -arvz public/expansions/ root@185.227.111.144:/var/www/robinzon-api/public/expansions/

echo '[+] Sync is complete'
